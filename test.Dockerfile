FROM python:3.6.1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD ./requirements/ /usr/src/app/requirements/

RUN pip install -r requirements/testing.txt

ADD . /usr/src/app
