from flask_migrate import MigrateCommand
from flask_script import Manager

from soyvoluntario import create_app, db

app = create_app()
manager = Manager(app)

manager.add_command('db', MigrateCommand)

@manager.command
def recreate_db():
    """Recreates a database"""
    db.drop_all()
    db.create_all()
    db.session.commit()

if __name__ == '__main__':
    manager.run()
