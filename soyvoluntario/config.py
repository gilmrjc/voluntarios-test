import os

class BaseConfig:
    """Base configuration"""
    BCRYPT_LOG_ROUNDS = 4
    DEBUG = False
    TESTING = False
    TOKEN_EXPIRATION_DAYS = 30
    SECRET_KEY = os.environ.get('SECRET_KEY', 'totally secure')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL',
                                             'sqlite:////tmp/soyvoluntario.db')

class TestingConfig(BaseConfig):
    """Testing configuration"""
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL')

class ProductionConfig(BaseConfig):
    """Production configuration"""
    BCRYPT_LOG_ROUNDS = 14
    TOKEN_EXPIRATION_DAYS = 1
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

settings = {
    'Development': DevelopmentConfig,
    'Testing': TestingConfig,
    'Production': ProductionConfig
}
