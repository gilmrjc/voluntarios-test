import os
import datetime

from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask import Flask, jsonify
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from soyvoluntario.config import settings

bcrypt = Bcrypt()
db = SQLAlchemy()
migrate = Migrate()

def create_app():
    app = Flask(__name__)

    app_settings = os.getenv('APP_SETTINGS', 'Development')
    app.config.from_object(settings[app_settings])

    CORS(app)

    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    from soyvoluntario.api.users import users_blueprint
    from soyvoluntario.api.auth import auth_blueprint
    app.register_blueprint(users_blueprint)
    app.register_blueprint(auth_blueprint)

    return app


