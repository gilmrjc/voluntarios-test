from flask import Blueprint, jsonify, request
from sqlalchemy import exc

from soyvoluntario import db
from soyvoluntario.api.models import User
from soyvoluntario.api.utils import authenticate

users_blueprint = Blueprint('users', __name__)

@users_blueprint.route('/ping', methods=['GET'])
def ping():
    return jsonify({
        'status': 'success',
        'message': 'pong!'
    })

@users_blueprint.route('/users', methods=['GET'])
def get_users():
    users = User.query.all()
    users_list = []
    for user in users:
        user_object = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'created_at': user.created_at,
        }
        users_list.append(user_object)
    response_object = {
        'status': 'success',
        'data': {
            'users': users_list,
        },
    }
    return jsonify(response_object), 200

@users_blueprint.route('/users/<user_id>', methods=['GET'])
def get_user(user_id):
    """Get single user details."""
    response_object = {
        'status': 'fail',
        'message': 'User does not exist'
    }
    try:
        user = User.query.filter_by(id=user_id).first()
        if user:
            response_object = {
                'status': 'success',
                'data': {
                    'username': user.username,
                    'email': user.email,
                    'created_at': user.created_at,
                }
            }
            return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404
    return jsonify(response_object), 404

@users_blueprint.route('/users', methods=['POST'])
@authenticate
def add_user():
    post_data = request.get_json()
    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        return jsonify(response_object), 400
    username = post_data.get('username')
    email = post_data.get('email')
    password = post_data.get('password')
    try:
        user = User.query.filter_by(email=email).first()
        if user:
            response_object = {
                'status': 'fail',
                'message': 'Sorry. That email already exists'
            }
            return jsonify(response_object), 400
        db.session.add(User(username=username, email=email, password=password))
        db.session.commit()
        response_object = {
            'status': 'success',
            'message': '{} was added!'.format(email)
        }
        return jsonify(response_object), 201
    except (exc.IntegrityError, ValueError) as e:
        db.session.rollback()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        return jsonify(response_object), 400
