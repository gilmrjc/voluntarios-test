FROM python:3.6.1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD ./requirements/ /usr/src/app/requirements/

RUN pip install -r requirements/production.txt

ADD . /usr/src/app

CMD python manage.py runserver -h 0.0.0.0
