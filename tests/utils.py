import datetime

from soyvoluntario.api.models import User
from soyvoluntario import db

def add_user(username, email, password, created_at=datetime.datetime.utcnow()):
    user = User(username=username, email=email, password=password,
                created_at=created_at)
    db.session.add(user)
    db.session.commit()
    return user
