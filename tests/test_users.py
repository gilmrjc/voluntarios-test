import json

from tests.base import BaseTestCase
from tests.utils import add_user

from soyvoluntario.api.models import User

class TestUserService(BaseTestCase):
    """"Test the Users service."""

    def test_users(self):
        response = self.client.get('/ping')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert 'pong!' in data['message']
        assert 'success' in data['status']

    def test_add_user(self):
        """Ensure a new user can be added to the database."""
        with self.client:
            response = self.client.post(
                '/users',
                data=json.dumps({
                    'username': 'michael',
                    'email': 'michael@realpython.com',
                    'password': 'test',
                }),
                content_type='application/json',
            )
            data = json.loads(response.data.decode())
            assert response.status_code == 201
            assert 'michael@realpython.com was added!' in data['message']
            assert 'success' in data['status']

    def test_add_user_invalid_json(self):
        response = self.client.post(
        '/users',
        data=json.dumps(dict()),
        content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert 'Invalid payload' in data['message']
        assert 'fail' in data['status']

    def test_add_user_invalid_json_keys(self):
        with self.client:
            response = self.client.post(
            '/users',
            data=json.dumps({
                'email': 'michael@realpython.com',
            }),
            content_type='application/json',
            )
            data = json.loads(response.data.decode())
            assert response.status_code == 400
            assert 'Invalid payload' in data['message']
            assert 'fail' in data['status']

    def test_add_user_duplicate_user(self):
        with self.client:
            self.client.post(
            '/users',
            data=json.dumps({
                'username': 'michael',
                'email': 'michael@realpython.com',
                'password': 'test',
            }),
            content_type='application/json',
            )
            response = self.client.post(
            '/users',
            data=json.dumps({
                'username': 'michael',
                'email': 'michael@realpython.com',
                'password': 'test',
            }),
            content_type='application/json',
            )
            data = json.loads(response.data.decode())
            assert response.status_code == 400
            assert "Sorry. That email already exists" in data['message']
            assert 'fail' in data['status']

    def test_single_user(self):
        """Ensure get single users behaves correctly."""
        user = add_user('michael', 'michael@realpython.com', 'test')
        with self.client:
            response = self.client.get('/users/{}'.format(user.id))
            data = json.loads(response.data.decode())
            assert response.status_code == 200
            assert 'created_at' in data['data']
            assert 'michael' in data['data']['username']
            assert 'michael@realpython.com' in data['data']['email']
            assert 'success' in data['status']

    def test_single_user_no_id(self):
        with self.client:
            response = self.client.get('/users/blah')
            data = json.loads(response.data.decode())
            assert response.status_code == 404
            assert 'User does not exist' in data['message']
            assert 'fail' in data['status']

    def test_single_user_incorrect_id(self):
        with self.client:
            response = self.client.get('/users/999')
            data = json.loads(response.data.decode())
            assert response.status_code == 404
            assert 'User does not exist' in data['message']
            assert 'fail' in data['status']

    def test_all_users(self):
        add_user('michael', 'michael@realpython.com', 'test')
        add_user('fletcher', 'fletcher@realpython.com', 'test')
        with self.client:
            response = self.client.get('/users')
            data = json.loads(response.data.decode())
            assert response.status_code == 200
            assert len(data['data']['users']) == 2
            assert 'michael' in data['data']['users'][0]['username']
            assert 'fletcher' in data['data']['users'][1]['username']
            assert 'success' in data['status']

    def test_passwords_are_random(self):
        user_one = add_user('test1', 'test1@domain.com', 'test')
        user_two = add_user('test2', 'test2@domain.com', 'test')
        assert user_one.password != user_two.password

    def test_encode_auth_token(self):
        user = add_user('test', 'test@domain.com', 'test')
        auth_token = user.encode_auth_token(user.id)
        assert isinstance(auth_token, bytes)

    def test_decode_auth_token(self):
        user = add_user('justatest', 'test@test.com', 'test')
        auth_token = user.encode_auth_token(user.id)
        assert isinstance(auth_token, bytes)
        assert User.decode_auth_token(auth_token) == user.id
